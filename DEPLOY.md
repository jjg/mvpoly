Distributing a new version
--------------------------

- Change the version in `CHANGES.md`
- Change the version and date in `configure.ac`
- Run `make update-autoconf`
- Run `./configure`
- In `src/python` set-up a virtual environment, then make the targets
  `build`, `upload-gitlab` (documentation is rebuild on push)
- In `src/octave` run `make package` and install in website
- In `src/matlab` likewise.
- Run `make git-release-commit`
- Run `make git-create-tag`
- Run `make git-push-tag`
