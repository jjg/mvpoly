Changes
-------

### 0.98.4 (2024-06-23)
#### Python

- Minor changes for compatibility with Numpy 2.0

### 0.98.3 (2023-10-02)
#### Python

- Replace call to `version_compare` in now-deprecated
  `pkg_resources` module by an onboard version, CI now
  runs against Python 3.12 without error

### 0.98.2 (2023-07-05)

- Documentation update

### 0.98.1 (2023-03-23)
#### Python

- Modernise the Python build/update (virtual environments,
  as PEP-517 as possible, switch docs to Read the Docs)
- Replace `scipy.linalg.eigh` keyword argument `eigvals` by
  `subset_by_index`

#### Octave

- Replace deprecated `flipdim` by `flip`

### 0.97.9 (2019-12-13)
#### Python

- Updated `setup.py` to handle broken SciPy 1.2.0.
- Version checking replaced by that in setuptools.
- Better handling of dtypes in MVPolyCube addition.

### 0.97.8 (2019-01-24)
#### Python

- Added `chsym`, complete homogeneous symmetric polynomials.
- Test suite switched to _pytest_.
- Updates for Numpy, SciPy, now tested on Python 3.6.

### 0.97.7 (2018-01-15)
#### Octave/Matlab

- Test that `maxmodnb` 1D input gives 1D output.

#### Python

- Instances respond to `repr()`.
- Hypothesis test suite.

### 0.97.6 (2017-07-23)
#### Octave/Matlab

- Added a wrapper for `maxmodnb`.

### 0.97.5 (2017-01-30)
#### Python

- Removed the "KD-tree" hack for the sparse matrix distance
  used in the Wendland RBF function, as a result this function
  now requires SciPy 0.17.0 (2016-01-23) or later.

### 0.97.4 (2017-01-28)
#### Python

- Fixed a `__setitem__` zero bug (`p[3] = 0`) in `MVPolyDict`.
- Minor fixes needed to run under Numpy 1.12.0, SciPy 0.18.1.

### 0.97.3 (2017-01-05)
#### Python

- Added the Bell complete and partial polynomials in the class
  methods `bell` and `bell_partial` respectively.
- Default radius for the Wendland RBF now depends on the
  dimension of the problem.

### 0.97.2 (2016-07-24)
#### Octave/Matlab

- Updated the library to use the _classdef_ object mechanism,
  leading to a completed revamp in the syntax (for example,
  `eval(p, x)` is replaced by more expressive `p.eval(x)`).
  Octave 4.0 or above is now required.
- The functions `order` and `homdeg` are replaced by the instance
  methods `degrees` and `degree`, for consistency with the Python
  package.
- Added the class methods `one`, `zero` and the instance methods
  `norm`, `height` and `width`.

#### Python

- Python code now checked by https://www.quantifiedcode.com/
- All RBF classes are now subclasses of either the _RBFDense_ or the
  _RBFSparse class_; for the latter the interpolation coefficients
  are found by inverting a naturally sparse system. At present the
  only such subclass is _RBFWendland_ whose instances interpolate with
  the Wendland compactly supported RBF.

### 0.97.1 (2015-11-18)
#### Python

- Added a `large` keyword-argument for Python RBF interpolation
  functions, de-vectorising the interpolation and so reducing memory
  at expense of speed --- useful when interpolating a large number
  of points.

### 0.97.0 (2015-11-03)
#### Python

- Added the Wendland compactly supported RBF (but sparsity not yet
  exploited).
