VERSION = $(shell cat VERSION)

all:
	$(MAKE) -C src all

install:
	$(MAKE) -C src install

clean:
	$(MAKE) -C src clean

spotless:
	$(MAKE) -C src spotless
	$(RM) config.log config.status
	rm -rf autom4te.cache

# autoconf

update-autoconf:
	aclocal --output config/aclocal.m4
	autoconf

# git tag handling

git-release-commit:
	git add -u
	git commit -m $(VERSION)
	git push origin master

git-create-tag:
	bin/git-release-tag $(VERSION)

git-push-tag:
	git push origin v$(VERSION)

.PHONY: git-release-commit git-create-tag git-push-tag

# debian depends

install-debian-depends:
	apt-get install $(shell cat config/debian.req)
