Developing
----------

First, configure with

    ./configure

this checks what's available on your machine and writes various
files.  To work on the Octave or Matlab code, change into those
directories and code away, the usual Makefile targets will work
(be aware that those use shared code in `src/m`).

For Python, things used to be rather similar, but now things are
done with virtual environments.  So after the configure, change
into the Python directory, run

    make venv-create

to create the environment,

    make venv

will show you a command to run to start the environment.

    make install-dev

will install development requirements into the environment

    make build

will create the package in `dist`.  To install

    make install

but that installs into the virtual environment -- if your aim
is to install the package into _your_ virtual environemt, then
replace the `make venv` etc commands with ones which gets you
into that environment.

You may want to run the tests:

    make install-test
    make test

modify the code, the tests and run `make test` again.  When done

    deactivate

gets you out of the virtual environment,

    make venv-destroy

gets rid of it.  Returning to the root of the repository and
running

    make spotless

will make things pristine.
