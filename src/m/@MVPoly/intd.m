function x = intd(p, varargin)
% INTD - definite integral of polynomial
%
% Given an k-variate polynomial object p, a call to
%
%    intd(p, [x1_min, x1max], ..., [xk_min, xk_max])
%
% will return the definite integral of p over the
% cube defined by the last k arguments. Alternatively
% one can call
%
%    intd(p, lim)
%
% where lim is the k x 2 matrix of limits
%
%    [x1_min  x1_max
%     x2_min  x2_max
%       :
%     xk_min  xk_max]
%
% to obtain the same result.
%
% The calculation is performed by evaluation of the
% indefinite integral rather than by quadratute.

    pv = degrees(p);
    pn = numel(pv);
    k = nargin - 1;

    switch k
      case 1
        lim = varargin{1};
        if (size(lim, 1) ~= pn) && (size(lim, 2) ~= 2)
            error('intd: expected 2nd arg to be %i x 2 matrix', pn)
        end
        x = intd1(p, lim);
      case pn
        lim = reshape(cell2mat(varargin), 2, pn).';
        x = intd1(p, lim);
      otherwise
        error('intd: expected %i arguments', pn + 1);
    end

end

function x = intd1(p, lim)

    pn = size(lim, 1);
    intp = int(p, ones(1, pn));

    v = lim(1,:);
    for i = 2:pn
        v = [repmat(v, 1, 2)
             kron(lim(i, :), ones(1, 2^(i-1)))];
    end

    if pn > 1
        c(1:pn) = {[-1, 1]};
        sgn = MVPoly.kronn(c{:});
    else
        sgn = [-1, 1];
    end

    b = eval(intp, v);
    x = sgn * b;

end
