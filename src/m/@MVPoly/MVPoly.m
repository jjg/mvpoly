classdef MVPoly
    methods (Access = public)
        x = intd(p, varargin)
        x = width(p)
        x = height(p)
        [M, t, h, evals] = maxmodnd(p, epsilon, verbose, i0)
        q = polyder(p, varargin)
        q = polyint(p, varargin)
        x = polyval(p, varargin)
    end

    methods (Static, Access = protected)
        a = kronn(b, c, varargin)
    end
end