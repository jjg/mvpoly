function q = polyint(p, varargin)
% POLYINT - integrate polynomial
%
% Alias for INT
    
    q = int(p, varargin{:});
    
end