function x = polyval(p, varargin)
% POLYVAL - evaluate multivatiate polynomial
%
% Alias for EVAL.

    x = p.eval(varargin{:});

end