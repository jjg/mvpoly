function a = kronn(b, c, varargin)
% KRONN - kron with arbitrarly many arguments
%
% A multi-argument version of kron, defined by
%
%   kronn(b, c, d) = kron(kron(b, c), d)
%
% and so on.  This is identical to the behaviour of
% Octave's built-in kron function, but not Matlab's.

    a = kron(b, c);

    for i = 1:(nargin-2)
        a = kron(a, varargin{i});
    end

end
