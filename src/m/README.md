MVPoly for Matlab/Octave
------------------------

This is the m-file directory for the MVPoly package for calulations
with multivariate polynomials in Matlab/Octave.

The code uses the classpath object interface of Matlab (and, since
version 4, Octave). The classes are defined in `@MVpoly` (the base
class) and `@MVPolyCube` (the subclass based on multidimensional
arrays, suitable for polynomials with a "dense" coefficient structure.

Unit tests are in the `mvpoly_unit` directory and are easiest run from
the shell scripts `mvpoly-unit-matlab` and `mvpoly-unit-octave`; these
handle the quirks of the command-lines of each interpreter.
