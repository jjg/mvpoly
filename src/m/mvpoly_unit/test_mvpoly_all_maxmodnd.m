function test_mvpoly_all_maxmodnd()
% test suite for @MVPoly/maxmodnb

    test_univariate;
    test_bivariate_I;
    test_bivariate_II;
    test_kaijser_varopoulos;

end

function test_univariate()
    p = MVPolyCube();
    p(0) = 1; p(10) = 1;
    M = p.maxmodnd(1e-9);
    assert(abs(M-2)/2 < 1e-9, 'univariate');
end

function test_bivariate_I()
    p = MVPolyCube();
    p(0, 0) = 8; p(0, 9) = 1; p(9, 9) = 1;
    M = p.maxmodnd(1e-9);
    assert(abs(M-10)/10 < 1e-9, 'bivariate I');
end

function test_bivariate_II()
    p = MVPolyCube(ones(3, 3, 3));
    M = p.maxmodnd(1e-9);
    assert(abs(M-27)/27 < 1e-9, 'bivariate II');
end

function test_kaijser_varopoulos()
    p = MVPolyCube();
    p(0, 2, 2) =  1;
    p(2, 0, 2) =  1;
    p(2, 2, 0) =  1;
    p(1, 1, 2) = -2;
    p(1, 2, 1) = -2;
    p(2, 1, 1) = -2;
    M = p.maxmodnd(1e-5);
    assert(abs(M-5)/5 < 1e-5, 'kaijser-varopoulos');
end