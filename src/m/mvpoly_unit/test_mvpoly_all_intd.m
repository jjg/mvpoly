function test_mvpoly_all_intd()
% test suite for @MVPoly/intd

    test_1d;
    test_2d;
    test_3d;

end

function test_1d()
    x = MVPolyCube.variables;
    assert( abs(x.intd([0, 1]) - 0.5) < eps, ...
            'variable x over [0, 1]');
    assert( abs(x.intd([1, 2]) - 1.5) < eps, ...
            'variable x over [1, 2]');
end

function test_2d()
    [x, y] = MVPolyCube.variables;
    p = x^2 + 4*y;
    assert( abs(p.intd([11, 14], [7,10]) - 1719) < eps, ...
            'wikipedia example');
end

function test_3d()
    [x, y, z] = MVPolyCube.variables;
    p = x + y + z;
    assert( abs(p.intd([0, 1], [0, 1], [0,1]) - 3/2) < eps, ...
            'wolfram alpha (1)');
    p = 3*x^2 + y + z;
    assert( abs(p.intd([0, 1], [2, 4], [-2, 0]) - 12) < eps, ...
            'wolfram alpha (2)');
end