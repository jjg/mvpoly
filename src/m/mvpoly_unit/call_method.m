function varargout = call_method(class_name, method_name, varargin)
% CALL_METHOD - call a class's method
%
% This works fine on Matlab, but is broken on Octave as of 4.0.2.
% If when that is fixed, and if we ever implement another subclass,
% then this could be used to DRY up test_mvpoly_all_*.m

    f = str2func(sprintf('%s.%s', class_name, method_name));
    [varargout{1:nargout}] = f(varargin{:});

end
