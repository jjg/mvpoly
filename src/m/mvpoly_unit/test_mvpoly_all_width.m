function test_mvpoly_all_width()
% test suite for @MVPoly/width

    test_zero;
    test_x;
    test_y;

end

function test_zero()
    p = MVPolyCube.zero;
    assert(p.width == 0, 'zero polynomial')
end

function test_x()
    [x, y] = MVPolyCube.variables;
    p = 3*x^2 - 4;
    assert(p.width == 7, 'polynomial in x')
end

function test_y()
    [x, y] = MVPolyCube.variables;
    p = 3*y^2 - 4;
    assert(p.width == 7, 'polynomial in y')
end