function test_mvpoly_cube_compose()
% test suite for @MVPolyCube/compose

    test_variable_exchange_I;
    test_variable_exchange_II;
    test_distribute_over_eval;
    test_differing_dimensions;

end

function test_variable_exchange_I()
    [x, y] = MVPolyCube.variables;
    p = MVPolyCube(rand(3));
    q = p.compose(x, y);
    assert(array_equal(p.coef, q.coef, eps), 'variable exchange I');
end

function test_variable_exchange_II()
    [x, y] = MVPolyCube.variables;
    p = MVPolyCube(rand(3));
    q = p.compose(y, x);
    assert(array_equal(p.coef, transpose(q.coef), eps),...
           'variable exchange II');
end

function test_distribute_over_eval()
    p = MVPolyCube(rand(3));
    x = MVPolyCube(rand(3));
    y = MVPolyCube(rand(3));
    v = rand(2, 1);
    A = p.compose(x, y).eval(v);
    B = p.eval(x.eval(v), y.eval(v));
    assert(abs((A - B)/(A + B)) < eps, 'distribute evaluate over compose');
end

function test_differing_dimensions()
    [x, y] = MVPolyCube.variables;
    p = x^2 + 5;
    q = x + y;
    r = p.compose(q);
    assert(array_equal(r.coef, ...
                       [5, 0, 1;
                        0, 2, 0;
                        1, 0, 0], eps), 'differing dimensions');
end