function test_mvpoly_cube_addition()
% test suite for @MVPolyCube/plus, @MVPolyCube/minus

    test_simple_sum;
    test_poly_plus_scalar;
    test_scalar_plus_poly;
    test_add_different_size;
    test_subtract_different_size;

end

function test_simple_sum()
    [p, q] = MVPolyCube.variables;
    r = p + q;
    assert(isa(r,'MVPolyCube'), 'class for simple sum');
    assert(array_equal(r.coef, [0, 1 ; 1, 0]), 'simple sum');
end

function test_poly_plus_scalar()
    [p, q] = MVPolyCube.variables;
    r = p + q + 1;
    assert(isa(r,'MVPolyCube'), 'class for poly plus scalar');
    assert(array_equal(r.coef, [1, 1 ; 1, 0]), 'poly plus scalar');
end

function test_scalar_plus_poly()
    [p, q] = MVPolyCube.variables;
    r = 1 + p + q;
    assert(isa(r,'MVPolyCube'), 'class for scalar plus poly');
    assert(array_equal(r.coef, [1, 1 ; 1, 0]), 'scalar plus poly');
end

function test_add_different_size()
    p = MVPolyCube(ones(2, 2, 2)) + MVPolyCube(ones(3, 3));
    assert(array_equal(size(p.coef), [3 3 2]), 'sum of different sizes');
end

function test_subtract_different_size()
    p = MVPolyCube(ones(2, 2, 2)) - MVPolyCube(ones(3, 3));
    assert(array_equal(size(p.coef), [3, 3, 2]), ...
           'difference of different sizes');
end