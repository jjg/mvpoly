function test_mvpoly_cube_zero()
% test suite for @MVPolyCube/zero

    test_constant_term;
    test_homogeneous_degree;
    test_degrees;

end

function test_constant_term()
    p = MVPolyCube.zero;
    assert(p(0) == 0, 'constant term is zero');
end

function test_homogeneous_degree()
    p = MVPolyCube.zero;
    assert(p.degree() < 0, 'homogeneous degree');
end

function test_degrees()
    p = MVPolyCube.zero;
    assert(array_equal(p.degrees(), [0], eps), 'degrees');
end