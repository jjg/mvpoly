function test_mvpoly_cube_subsasgn()
% test suite for @MVPolyCube/subsasgn

    test_explicit_assign;
    test_implicit_zero;
    test_block_assign_array;
    test_block_assign_scalar;

end

function test_explicit_assign()
    p = MVPolyCube;
    p(1, 2, 3) = 1;
    assert(p(1, 2, 3) == 1, 'element assign explicit coefficient');
end

function test_implicit_zero()
    p = MVPolyCube;
    p(1, 2, 3) = 1;
    assert(p(1, 1, 2) == 0, 'element assign implicit coefficient');
end

function test_block_assign_array()
    p = MVPolyCube;
    p(0:1, 0:1) = eye(2);
    assert(array_equal(p.coef, eye(2)), 'block assign array');
end

function test_block_assign_scalar()
    p = MVPolyCube;
    p(0:1, 0:1) = 1;
    assert(array_equal(p.coef, ones(2)), 'block assign scalar');
end
