function test_mvpoly_cube()
% test suite for @MVPolyCube/MVpolyCube

    test_without_arguments;
    test_with_argument;
    test_coef_retention;
    test_traditional_option;

end

function test_without_arguments()
    p = MVPolyCube;
    assert(isa(p, 'MVPolyCube'), 'constructor without argument');
end

function test_with_argument()
    p = MVPolyCube(ones(2));
    assert(isa(p, 'MVPolyCube'), 'constructor with argument');
end

function test_coef_retention()
    a = rand(5, 5, 5);
    p = MVPolyCube(a);
    assert(array_equal(p.coef, a), 'constructor data retention');
end

function test_traditional_option()
    a = [1 2 ; 3 4];
    b = [4 3 ; 2 1];
    p = MVPolyCube(a, 'traditional');
    assert(array_equal(p.coef, b), 'traditional option');
end