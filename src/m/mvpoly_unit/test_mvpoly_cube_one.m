function test_mvpoly_cube_one()
% test suite for @MVPolyCube/one

    test_constant_term;
    test_homogeneous_degree;
    test_degrees;

end

function test_constant_term()
    p = MVPolyCube.one;
    assert(p(0) == 1, 'constant term is one');
end

function test_homogeneous_degree()
    p = MVPolyCube.one;
    assert(p.degree() == 0, 'homogeneous degree');
end

function test_degrees()
    p = MVPolyCube.one;
    assert(array_equal(p.degrees(), [0], eps), 'degrees');
end