function test_mvpoly_cube_norm()
% test suite for @MVPolyCube/norm

    test_zero;
    test_univariate;
    test_bivariate;

end

function test_zero()
    p = MVPolyCube.zero;
    assert(p.norm(1) == 0, '1 norm');
    assert(p.norm(2) == 0, '2 norm');
    assert(p.norm(Inf) == 0, 'infinity norm');
end

function test_univariate()
    x = MVPolyCube.variables;
    p = 3*x^2 + 4;
    assert(p.norm(1) == 7, '1 norm');
    assert(p.norm(2) == 5, '2 norm');
    assert(p.norm(Inf) == 4, 'infinity norm');
end

function test_bivariate()
    [x, y] = MVPolyCube.variables;
    p = 3*x^2 + 4*y;
    assert(p.norm(1) == 7, '1 norm');
    assert(p.norm(2) == 5, '2 norm');
    assert(p.norm(Inf) == 4, 'infinity norm');
end