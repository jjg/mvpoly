function test_mvpoly_cube_diff()
% test suite for @MVPolyCube/diff

    test_zero_dimension;
    test_zero;
    test_constant;
    test_univariate;
    test_bivariate_x;
    test_bivariate_y;

end

function test_zero_dimension()
    p = MVPolyCube([]);
    q1 = p.diff(1);
    assert(array_equal(q1.coef, []), 'zero-dimensional');
end

function test_zero()
    p = MVPolyCube([0]);
    q1 = p.diff(1);
    assert(array_equal(q1.coef, []), 'zero');
end

function test_constant()
    p = MVPolyCube([1]);
    q1 = p.diff(1);
    assert(array_equal(q1.coef, []), 'constant');
end

function test_univariate()
    x = MVPolyCube.variables;
    p = 2*x^2 + x + 1;
    q1 = p.diff(1);
    q2 = 4*x + 1;
    assert(array_equal(q1.coef, q2.coef), 'univariate');
end

function test_bivariate_x()
    [x, y] = MVPolyCube.variables;
    p = 3*x^2*y^2 + x - y + 10;
    q1 = p.diff(1, 0);
    q2 = 6*x*y^2 + 1;
    assert(array_equal(q1.coef, q2.coef), 'bivariate x');
end

function test_bivariate_y()
    [x, y] = MVPolyCube.variables;
    p = 3*x^2*y^2 + x - y + 10;
    q1 = p.diff(0, 1);
    q2 = 6*x^2*y - 1;
    assert(array_equal(q1.coef, q2.coef), 'bivariate y');
end
