function test_mvpoly_cube_degree()
% test suite for @MVPolyCube/degree

    test_zero;
    test_constant;
    test_non_constant;

end

function test_zero()
    p = MVPolyCube();
    h = p.degree();
    assert(h < 0, 'zero');
end

function test_constant()
    p = MVPolyCube([9]);
    h = p.degree;
    assert(h == 0, 'constant');
end

function test_non_constant()
    p = MVPolyCube;
    p(10, 1) = 1;
    p(6, 6) = 1;
    p(10, 10) = 0;
    h = p.degree;
    assert(h == 12, 'nontrivial');
end