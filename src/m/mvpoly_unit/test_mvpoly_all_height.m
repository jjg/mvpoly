function test_mvpoly_all_height()
% test suite for @MVPoly/height

    test_zero;
    test_x;
    test_y;

end

function test_zero()
    p = MVPolyCube.zero;
    assert(p.height == 0, 'zero polynomial')
end

function test_x()
    [x, y] = MVPolyCube.variables;
    p = 3*x^2 - 4;
    assert(p.height == 4, 'polynomial in x')
end

function test_y()
    [x, y] = MVPolyCube.variables;
    p = 3*y^2 - 4;
    assert(p.height == 4, 'polynomial in y')
end