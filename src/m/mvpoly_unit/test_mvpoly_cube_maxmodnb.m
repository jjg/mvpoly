function test_mvpoly_cube_maxmodnb()
% test suite for @MVPolyCube/maxmodnb

    if exist('maxmodnb_cube')

        test_nargout_1;
        test_nargout_2;
        test_nargout_3;

    else

        test_raises_error;

    end

end

function test_nargout_1()
    p = MVPolyCube();
    p(0) = p(5) = 1;
    epsilon = 1e-10;

    M = p.maxmodnb(epsilon);
    assert(abs(M - 2) < epsilon, 'nargout 1');
end

function test_nargout_2()
    p = MVPolyCube();
    p(0) = p(5) = 1;
    epsilon = 1e-10;

    [M, v] = p.maxmodnb(epsilon);
    assert(abs(M - 2) < epsilon, 'nargout 2 M');
    assert(array_equal(size(v), [1, 1], eps), 'nargout 2 v size');
    assert(abs(p.eval(v)) - 2 < 1e-7, 'nargout 2 v');
end

function test_nargout_3()
    p = MVPolyCube();
    p(0) = p(5) = 1;
    epsilon = 1e-10;

    [M, v, nevals] = p.maxmodnb(epsilon);
    assert(abs(M - 2) < epsilon, 'nargout 3 M');
    assert(array_equal(size(v), [1, 1], eps), 'nargout 3 v size');
    assert(abs(p.eval(v)) - 2 < 1e-7, 'nargout 3 v');
    assert(nevals > 0, 'nargout 3 nevals');
end

function test_raises_error()
    p = MVPolyCube(rand(5));
    try
        p.maxmodnb();
        raised = false;
    catch
        raised = true;
    end

    if ~ raised
        assert(false, 'Expected error not raised');
    end
end