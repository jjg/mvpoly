function test_mvpoly_cube_degrees()
% test suite for @MVPolyCube/degrees

    test_zero;
    test_zero_multivariate;
    test_univariate;
    test_faux_bivariate;
    test_trivariate;

end

function test_zero()
    p = MVPolyCube.zero;
    assert(array_equal(p.degrees, [0]), 'zero');
end

function test_zero_multivariate()
    p = MVPolyCube(zeros(4, 4, 4));
    assert(array_equal(p.degrees, [0, 0, 0]), 'zero multivariate');
end

function test_univariate()
    x = MVPolyCube.variables;
    p = x^12;
    assert(array_equal(p.degrees(), [12]), 'univariate');
end

function test_faux_bivariate()
    [x, y] = MVPolyCube.variables;
    p = y^5 + 1;
    assert(array_equal(p.degrees, [0, 5]), 'faux bivariate');
end

function test_trivariate()
    [x, y, z] = MVPolyCube.variables;
    p = x * (1 + y^2 * z^3);
    assert(array_equal(p.degrees, [1, 2, 3]), 'trivariate');
end
