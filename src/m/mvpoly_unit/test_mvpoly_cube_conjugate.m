function test_mvpoly_cube_conjugate()
% test suite for @MVPolyCube/ctranspose

    test_real_random;
    test_complex_example;

end

function test_real_random()
    p = MVPolyCube(rand(5));
    q = p';
    assert(array_equal(p.coef, q.coef), 'real random');
end

function test_complex_example()
    [x, y] = MVPolyCube.variables;
    p = i*x - i*y + 1;
    q1 = p';
    q2 = -i*x + i*y + 1;
    assert(array_equal(q1.coef, q2.coef), 'complex explicit');
end