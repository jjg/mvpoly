function varargout = subsref(p, s)
% SUBSREF - subscript access to multivariate polynomial
%
% The call p(a, b,..) access the coefficient determined
% by a, b, ..., which may be ranges (such as 1:5) or the
% "magic colon", returning the full range of that variable.
%
% The "brace subscripts" p{} are not defined for
% MVPolyCube objects.
%
% See also subsref, subsasgn, @MVPolyCube/subsasgn

    if (isempty(s))
        error('@MVPolyCube/subsref: missing index');
    end

    switch (s(1).type)

      case '()'
        subs = subsnorm(cellfun(@exp2sub, s(1).subs, 'UniformOutput', false));
        s(1).subs = subs;
        [varargout{1}] = subsref(p.coef, s);

      case '.'
        [varargout{1:nargout}] = builtin('subsref', p, s);

      otherwise
        error('invalid subscript type');

    end

end
