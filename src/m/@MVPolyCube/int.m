function q = int(p, varargin)
% INT - indefinite integral of multivariate polynomial
%
% A call to
%
%    p.int(n1, n2, ..., nk)
%
% where k is the number of variables of the polynomial
% p, returns the repeated indefinite integral of p with
% respect to the n-th variables without the arbitrary
% constant (or rather the arbitrary function of the other
% variables) of integration. So
%
%    [x, y] = MVPolyCube.variables();
%    p = x + 2*y;
%    p.int(1, 0)
%
% returns the polynomial representing
%
%   /                1  2
%   |  x + 2y dx  =  - x  + 2xy
%   /                2
%
% If any of the n1,..,nk are larger than 1 then the
% integration on that variable is repeated that many
% times.  Equivalently, one can call
%
%    p.int([n1, ..., nk])
%
% to get the same result.

    pv = degrees(p);
    pn = numel(pv);
    k = nargin - 1;

    switch k
      case 1
        d = varargin{1};
        if pn == numel(d);
            q = int1(p, d);
        else
            error('int: expected 2nd arg to be %i-vector', pn);
        end
      case pn
        d = cell2mat(varargin);
        q = int1(p, d);
      otherwise
        error('int: expect %i arguments', pn + 1);
    end

end

function q = int1(p, d)

    q = p;
    k = numel(d);

    for n = 1:k
        for i = 1:d(n)
            q = int2(q, n);
        end
    end

end

function q = int2(p, n)

    pc  = p.coef;
    szp = size(pc);
    m   = numel(szp);

    if (n > m) || (n < 1)
        error('polyomial does not have a %ith variable', n);
    end

    szq = szp;
    szq(n) = szq(n) + 1;
    qc = zeros(szq);

    idx = colons(m);
    idx{n} = 2:szq(n);

    qc(idx{:}) = pc ./ ndgridn(szp, n, 1:szp(n));

    q = MVPolyCube(qc);

end
