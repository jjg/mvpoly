function p = one()
% ONE - the polynomial with value 1

    p = MVPolyCube([1]);

end
