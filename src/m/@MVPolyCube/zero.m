function p = zero()
% ZERO - the zero polynomial

    p = MVPolyCube([0]);

end
