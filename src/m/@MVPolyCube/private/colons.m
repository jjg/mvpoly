function c = colons(n)
% COLONS - return a cell-array of n colons
    
    c(1:n) = {':'};

end
