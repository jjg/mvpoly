function v = degrees(p)
% DEGREES - degrees of variables in a multivariate polynomial
%
% The degree of a variable in a polynomial is the largest
% exponent of that variable occurring in monomials with
% non-zero coefficients. Thus x y^3 + x^2 y^2 has degree
% 2 in x and 3 in y.  This function returns the degrees of
% the variables of its argument in a vector.
%
% This function will return [0] if passed the zero polynomial.
%
% See also degree

    sz = size(p.coef);
    nd = numel(sz);
    sub = cell(nd, 1);
    [sub{:}] = ind2sub(sz, find(p.coef));
    for i = 1:numel(sub)
        w(i, :) = sub{i}(:)';
    end
    v = max(w', [], 1) - 1;
    switch numel(v)
      case 0
        v = [0];
      case 2
        if v(2) == 0
            v = [v(1)];
        end
    end

end