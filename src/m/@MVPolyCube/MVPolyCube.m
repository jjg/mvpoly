classdef MVPolyCube < MVPoly

    properties
        coef
    end

    methods
        function p = MVPolyCube(a, option)
        % MVPOLYCUBE - Create multivariate polynomial
        %
        % Creates a multivariate polynomial object with an ND-array
        % used to store the coefficients, this is for dense
        % polyomials, those with few zero coefficients. The call
        %
        %   p = MVPolyCube(a)
        %
        % creates a polynomial p with coefficients given by the
        % n-dimensional arrary a. In contrast to the convention
        % used in Matlab's polyval, the constant term is a(1, 1,..,)
        % and a(n, m, ...) corresponds to the coefficient of
        % x^(n+1) y^(m+1) ... However, it is more intuitive to assign
        % the coefficients with the MVPolyCube assignment method:
        %
        %   p = MVPolyCube(eye(2, 2))
        %
        % and
        %
        %   p = MVPolyCube(); p(0, 0) = p(1, 1) = 1;
        %
        % produce the same object.
        %
        % Since several Matlab/Octave libraries use an internal
        % representation of polynomials where the highest-order
        % coefficient is first in the array the constructor has
        % an option 'traditional' which flips all of the
        % dimensions of the coefficient array, thus
        %
        %   coefs = [1 2
        %            3 0];
        %   p = MVPolyCube(coefs)
        %
        % and
        %
        %   coefs = [0 3
        %            2 1];
        %   p = MVPolyCube(coefs, 'traditional')
        %
        % produce the same polynomial p(x, y) = 1 + 3x + 2y.
        %
        % See also @MVPolyCube/subsasgn

            if nargin == 0
                p.coef = [0];
            elseif nargin == 1
                if isa(a, 'MVPolyCube')
                    p = a;
                elseif isnumeric(a)
                    p.coef = a;
                else
                    error('%s: bad constructor argument', classname);
                end
            elseif nargin == 2
                if strcmp(option, 'traditional')
                    if isnumeric(a)
                        nd = numel(size(a));
                        for i = 1:nd
                            a = flip(a, i);
                        end
                        p = MVPolyCube(a);
                    else
                        error('option %s needs numeric constructor', option);
                    end
                else
                    error('unknown option %s', option);
                end
            else
                print_usage();
            end
        end

        % prototypes for other public methods

        q = compose(p, varargin)
        q = ctranspose(p)
        d = degree(p)
        v = degrees(p)
        q = diff(p, varargin)
        display(p)
        y = eval(p, varargin)
        q = int(p, varargin)
        [M, v, nevals] = maxmodnb(p, varargin)
        c = minus(a, b)
        q = mpower(p, n)
        q = mtimes(p1, p2)
        x = norm(p, mu)
        q = plus(p1, p2)
        x = subsasgn(x, ss, val)
        q = subsref(p, s)
        b = uminus(a)

    end

    methods (Static)
        [varargout] = variables()
        p = one()
        p = zero()
    end
end
