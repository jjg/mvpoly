function [varargout] = variables()
% VARIABLES - polynomial variables
%
% A call to
%
%    [x, y, z] = MVPolyCube.variables()
%
% return MVPolyCube objects representing the variables
% x, y and z.  This is useful in constucting multivariate
% polynomials in an "almost symbolic" fashion.

    idx.type = '()';
    idx.subs = cell(1);
    for i = 1:nargout
        idx.subs{i} = 1;
        varargout{i} = subsasgn(MVPolyCube(), idx, 1);
        idx.subs{i} = 0;
    end
end
