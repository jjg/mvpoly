function [M, v, neval] = maxmodnb(p, varargin)
% MAXMODNB - evaluate polynomial maximum modulus on unit ball
%
% If p is an n-variate polynomial (i.e., of the class mvpoly_cube,
% then the call
%
%   [M, v, neval] = maxmodnb(p, epsilon)
%
% will evaluate the maximum modulus of the polynomial p on the
% complex n-ball, using the method of De La Chevrotière, Drury
% and Green.
%
% The argument epsilon is the (relative) accuracy required, and
% the return values are M, the maximum modulus, v, a maximising
% (complex) vector, and neval, the number of polynomials used in
% finding the estimate.
%
% References
%
% [1] G. De La Chevrotière. "Finding the maximum modulus of a
%     polynomial on the polydisk using a generalization of
%     Steckin's lemma", SIAM Undergrad. Res. Online, 2, 2009.
%
% [2] S.W. Drury. "Steckin type estimates on the ball",
%     unpublished note, 2013
%
% [3] J.J. Green. "Calculating the maximimum modulus of a polynomial
%     with Steckin's lemma". SIAM J. Numer. Anal., 83:211-222, 2000.
%
% This function is a wrapper around the maxmodnb_cube oct-file
% which acts on the array of coefficients which represent the
% polynomial p (i.e., the base data structure of the mvpoly_cube
% object).

    if ~ exist('maxmodnb_cube')
        error('maxmodnb_cube extension not installed')
    end

    [M, v, neval] = maxmodnb_cube(p.coef, varargin{:});

end