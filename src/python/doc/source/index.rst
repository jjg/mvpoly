mvpoly
======

A package for the numeric treatment of multivariate
polynomials.

Contents:

.. toctree::

   Overview <intro>
   Usage <usage>
   The MVPoly base class <base>
   The MVPolyCube subclass <cube>
   The MVPolyDict subclass <dict>
   Radial basis functions: Theory <rbf_theory>
   Radial basis functions: Implementation <rbf_implement>
   Notes on the Octave/Matlab package <octmat>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
