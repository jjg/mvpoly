Radial basis functions: Implementation
======================================

In the interface to the library we break somewhat with the
mathematical notation of the previous section; the set of
nodes :math:`\mathbf{x}_i (i = 1, 2, \ldots, n)` is represented
as a set of (NumPy) arrays of the *coordinates* of the nodes.
So a (mathematical) set of *n* 3-dimensional nodes would be
represented as 3 NumPy arrays (*x*, *y*, *z* say), each with
*n* elements.

The library does not care a jot what *shape* the arrays
*x*, *y*, *z* is (they are flattened to 1-dimensional
vectors internally in any case); except that they are the
same shape.  This is a common representation in interpreted
languages (Python, Matlab, Octave, ...) so there is a certain
amount of library support for it: see NumPy's
`meshgrid <http://docs.scipy.org/doc/numpy/reference/generated/numpy.meshgrid.html>`_,
for example.

.. _class-rbf-base:

The :class:`RBFBase` class
--------------------------

.. autoclass:: mvpoly.rbf.RBFBase
   :members:
   :special-members:

.. _class-rbf-gaussian:

The :class:`RBFGaussian` subclass
---------------------------------

.. autoclass:: mvpoly.rbf.RBFGaussian
   :members:

.. _class-rbf-multiquadric:

The :class:`RBFMultiQuadric` subclass
-------------------------------------

.. autoclass:: mvpoly.rbf.RBFMultiQuadric
   :members:

.. _class-rbf-inverse-multiquadric:

The :class:`RBFInverseMultiQuadric` subclass
--------------------------------------------

.. autoclass:: mvpoly.rbf.RBFInverseMultiQuadric
   :members:

.. _class-rbf-thin-plate-spline:

The :class:`RBFThinPlateSpline` subclass
----------------------------------------

.. autoclass:: mvpoly.rbf.RBFThinPlateSpline
   :members:

The :class:`RBFWendland` subclass
---------------------------------

.. autoclass:: mvpoly.rbf.RBFWendland
   :members:
