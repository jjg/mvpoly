The :class:`MVPoly` base class
==============================

The base class :class:`MVPoly` is not to be instantiated
directly, instead one creates an object from a subclass,
such at the :class:`MVPolyCube` class.  However, the base
class does have methods -- operations on polynomials using
the restricted set of basic operations described above.
These methods are privileged, since they will work with 
all of the subclasses.

The base class also has class-methods (inherited by 
subclasses) which generate various interesting polynomials.

.. autoclass:: mvpoly.base.MVPoly
   :members:




