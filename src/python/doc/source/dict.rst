.. _class-dict:

The :class:`MVPolyDict` class
=============================

This class represents a mutivariate polynomial (typically sparse)
with a Python *dictionary*;  The keys represent the monomials of
the polynomials (see the
:class:`~mvpoly.dict.MVPolyDictMonomial`
class below) and the values, the non-zero coefficients.

The advantage of this sparse representation can be seen in the output
of the following script which confirms the Euler four-square identity,
discussed in 1749 in a letter from Euler to Goldbach ::

 import time
 from mvpoly.cube import *
 from mvpoly.dict import *

 for c in [MVPolyCube, MVPolyDict] :
     t0 = time.time()
     a1, a2, a3, a4, b1, b2, b3, b4 = c.variables(8)
     p = (a1**2 + a2**2 + a3**2 + a4**2) * (b1**2 + b2**2 + b3**2 + b4**2)
     q = (a1*b1 - a2*b2 - a3*b3 - a4*b4)**2 + \
         (a1*b2 + a2*b1 + a3*b4 - a4*b3)**2 + \
         (a1*b3 - a2*b4 + a3*b1 + a4*b2)**2 + \
         (a1*b4 + a2*b3 - a3*b2 + a4*b1)**2
     assert p == q, "failed Euler-Goldbach"
     t1 = time.time()
     print "%s %8.6f" % (c.__name__, t1 - t0)

which gives ::

  MVPolyCube 0.254305
  MVPolyDict 0.001889

indicating a speedup better than two orders of magnitude.

Method list
-----------

A list of all methods defined by the subclass. Naturally, the
methods of the base class are also available.

.. autoclass:: mvpoly.dict.MVPolyDict
   :members:
   :exclude-members: dtype_set_callback, rmf_eval
   :special-members:

Dictionary functions
--------------------

Library code for the detailed work with the dictionaries
is in the module :mod:`mvpoly.utils.dict`, it is not expected
that these functions will be called directly.

.. automodule:: mvpoly.util.dict
   :members:
   :special-members:

Monomial class
--------------

The keys of the :class:`MVPolyDict` dictionaries represent the
monomials of the polynomial.

.. autoclass:: mvpoly.dict.MVPolyDictMonomial
   :members:
   :exclude-members: __weakref__
   :special-members:
