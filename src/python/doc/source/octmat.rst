.. _section-octmat:

Notes on the Octave/Matlab versions of the package
==================================================

Most of the functionality of the Python implementation of dense
multivariate polynomials (:class:`~mvpoly.cube.MVPolyCube`) is
also available in the Octave and Matlab versions of the package.

The syntax differs, obviously ::

  #! /usr/bin/octave -q

  # create by initialising with a matrix or array
  p = MVPolyCube(ones(3));

  # create with monomials
  [x, y] = MVPolyCube.variables
  q = 1 + x*y + (x*y)^2

Cross-package
-------------

The Octave and Matlab packages share the same m-files, only
the documentation and install scripts differ.  This imposes
some limitations on the coding:

- Octave largely supports Matlab syntax while extending it:
  we make no use of these extensions since we must be Matlab
  compatible (so Matlab comments, help-page layout and so on).

- In the case that a function (or a function's behaviour) in
  Octave is indispensible, we include an implementation so that
  this is also available in Matlab (see the multi-argument
  version of :meth:`kron`, for example).

No sparse polynomials
---------------------

The sparse functionality (:class:`~mvpoly.dict.MVPolyDict`) is not
included in the Matlab/Octave packages due to the lack of a suitable
basic data-type on which to build it:

- Unlike Python, neither package has a dictionary type which
  can take integer *n*-tuples as keys; the closest thing is Matlab's
  `containers.Map <http://www.mathworks.co.uk/help/matlab/map-containers.html>`_,
  which can have character arrays as keys, but it is not yet
  implemented Octave.

- Neither package has a sparse N-dimensional array type; several
  authors have implemented such by "reshaping a sparse matrix",
  but this imposes limitations on the dimensions.

I would not expect the work involved in working-around these
limitations would be worth the effort.
