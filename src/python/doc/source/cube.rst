.. _class-cube:

The :class:`MVPolyCube` class
=============================

This class represents a mutivariate polynomial
with a Numpy N-dimensional array, a :class:`numpy.ndarray`.
The array holds the coefficients of the polynomial in
the natural, little-endian [#endian]_ fashion.

.. [#endian]
   A little-endian representation holds the
   coefficient for :math:`x^n y^m\cdots` in the
   :math:`(n,m,\ldots)` th element of the array.


Method list
-----------

A list of all methods defined by the subclass. Naturally, all of
the methods of the base class are also available.

.. autoclass:: mvpoly.cube.MVPolyCube
   :members:
   :exclude-members: dtype_set_callback
   :special-members:


Utility functions
-----------------

Library code for the serious work on the arrays is to be found
in the module :mod:`mvpoly.utils.cube`, it is not expected
that these functions will be called directly.

.. automodule:: mvpoly.util.cube
   :members:
