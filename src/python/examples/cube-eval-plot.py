# plot a polynomial on the unit square, matplotlib is
# needed for this script

import sys
sys.path.append('..')

import numpy as np
import mvpoly.cube as mvpc

x, y = mvpc.MVPolyCube.variables(2)
p = x*(1-x)*y*(1-y)

L = np.linspace(0, 1, 50)
Gx, Gy = np.meshgrid(L, L)
Gp = p(Gx, Gy)

from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(Gx, Gy, Gp, rstride=1, cstride=1, linewidth=0.1,
                       cmap=cm.coolwarm, antialiased=True)
bgcol='#333333'
fgcol='#cccccc'

ax.set_axis_bgcolor(bgcol)
ax.tick_params(axis='both', labelcolor=fgcol, color=fgcol)
ax.set_zlim(-0.01, 0.10)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

plt.show()
