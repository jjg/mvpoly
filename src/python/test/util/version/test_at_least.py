import pytest
from mvpoly.util.version import at_least


def test_scipy():
    assert at_least('scipy',  '0.0.1')
    assert not at_least('scipy',  '100.0.0')


def test_numpy():
    assert at_least('numpy',  '0.0.1')
    assert not at_least('numpy',  '100.0.0')
