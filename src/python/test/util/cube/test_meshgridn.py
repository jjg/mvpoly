import numpy as np
import pytest
from mvpoly.util.cube import meshgridn


def test_simple():
    L = np.array(range(3), dtype=int)
    G = np.meshgrid(L, L, indexing='ij')
    for i in range(2):
        Gi = meshgridn((3, 3), i, L)
        assert (Gi == G[i]).all()
