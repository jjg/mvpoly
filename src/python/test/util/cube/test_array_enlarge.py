import numpy as np
import pytest
from mvpoly.util.cube import array_enlarge


def test_simple():
    A = np.array(
        [[1, 2],
         [3, 4]],
        dtype=int
    )
    Bo = array_enlarge(A, (3, 4))
    Bx = np.array(
        [[1, 2, 0, 0],
         [3, 4, 0, 0],
         [0, 0, 0, 0]],
        dtype=int
    )
    assert (Bo == Bx).all()
    assert Bo.dtype == Bx.dtype


def test_badsize():
    A = np.array(
        [[1, 2],
         [3, 4]],
        dtype=int
    )
    with pytest.raises(ValueError):
        array_enlarge(A, (1, 4))
    with pytest.raises(ValueError):
        array_enlarge(A, (4, 1))
