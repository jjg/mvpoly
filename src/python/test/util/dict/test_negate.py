import pytest
from mvpoly.util.dict import negate


def test_simple():
    assert negate({'a': 2, 'b': -7}) == {'a': -2, 'b': 7}


def test_empty():
    assert negate({}) == {}
