import pytest
from mvpoly.util.dict import merge_dicts_nonzero


def test_simple():
    assert merge_dicts_nonzero(
        {'a': 3, 'b': 2, 'c': 1},
        {'b': 1, 'c': 2, 'd': 3},
        lambda x, y: x + y
    ) == {'a': 3, 'b': 3, 'c': 3, 'd': 3}


def test_cancel():
    assert merge_dicts_nonzero(
        {'a': 3, 'b': 2, 'c': 1},
        {'b': 1, 'c': 2, 'd': 3},
        lambda x, y: x - 2 * y
    ) == {'a': 3, 'c': -3, 'd': -6}
