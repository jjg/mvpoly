import numpy as np
from mvpoly.util.common import kronn
import pytest


def test_simple():
    a = np.array([1, -1])
    b = np.array([1, 1])
    expected = np.array([1, 1, 1, 1, -1, -1, -1, -1])
    obtained = kronn(a, b, b)
    assert (expected == obtained).all()


def test_2_arg():
    a = np.array([2, 3])
    b = np.array([4, 1])
    expected = np.kron(a, b)
    obtained = kronn(a, b)
    assert (expected == obtained).all()


def test_dtype():
    for dt in (int, float):
        a = np.array([[2, 3], [4, 5]], dtype=dt)
        expected = dt
        obtained = kronn(a, a, a).dtype
        assert expected == obtained
