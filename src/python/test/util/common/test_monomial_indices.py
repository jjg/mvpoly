from mvpoly.util.common import monomial_indices
import pytest


def test_count():
    data = {
        (1, 0): 1,
        (1, 1): 2,
        (1, 2): 3,
        (3, 0): 1,
        (3, 1): 4,
        (3, 2): 10,
        (3, 3): 20,
        (3, 4): 35,
        (4, 0): 1,
        (4, 1): 5,
        (4, 2): 15,
        (4, 3): 35,
        (4, 4): 70,
    }
    for arg, expected in data.items():
        obtained = len(list(monomial_indices(*arg)))
        assert expected == obtained


def test_explicit():
    data = {
        (1, 0): [(0,)],
        (1, 1): [(0,), (1,)],
        (1, 2): [(0,), (1,), (2,)],
        (2, 0): [(0, 0)],
        (2, 1): [(0, 0), (1, 0), (0, 1)],
        (2, 2): [(0, 0), (1, 0), (0, 1), (2, 0), (1, 1), (0, 2)],
    }
    for arg, expected in data.items():
        obtained = list(monomial_indices(*arg))
        assert expected == obtained


def test_bad_num_vars():
    for n in (-2, -1, 0):
        with pytest.raises(ValueError):
            list(monomial_indices(n, 2))


def test_bad_max_order():
    for k in (-3, -2, -1):
        with pytest.raises(ValueError):
            list(monomial_indices(2, k))
