import numpy as np
from mvpoly.util.common import as_numpy_scalar
import pytest


def test_int64():
    a = as_numpy_scalar(3)
    assert isinstance(a, np.int64)
    assert a == 3


def test_float64():
    a = as_numpy_scalar(3.0)
    assert isinstance(a, np.float64)
    assert a == 3.0


def test_kwargs_dtype_int64():
    a = as_numpy_scalar(3.0, dtype=np.int64)
    assert isinstance(a, np.int64)
    assert a == 3


def test_kwargs_dtype_float64():
    a = as_numpy_scalar(3, dtype=np.float64)
    assert isinstance(a, np.float64)
    assert a == 3
