from mvpoly.util.common import coerce_tuple
import pytest


def test_scalar():
    a = coerce_tuple(1)
    assert isinstance(a, tuple)
    assert a == (1,)


def test_string():
    a = coerce_tuple("froob")
    assert isinstance(a, tuple)
    assert a == ("froob",)


def test_1_tuple():
    a = coerce_tuple((1,))
    assert isinstance(a, tuple)
    assert a == (1,)


def test_n_tuple():
    a = coerce_tuple((1, 2, 3))
    assert isinstance(a, tuple)
    assert a == (1, 2, 3)


def test_1_list():
    a = coerce_tuple([1])
    assert isinstance(a, tuple)
    assert a == (1,)


def test_n_list():
    a = coerce_tuple([1, 2, 3])
    assert isinstance(a, tuple)
    assert a == (1, 2, 3)


def test_1_set():
    a = coerce_tuple(set([1]))
    assert isinstance(a, tuple)
    assert a == (1,)


def test_n_set():
    a = coerce_tuple(set([1, 2, 3]))
    assert isinstance(a, tuple)
    assert a == (1, 2, 3)
