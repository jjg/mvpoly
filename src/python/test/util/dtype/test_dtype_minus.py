import numpy as np

import pytest
from fixtures import cases

from mvpoly.util.dtype import dtype_minus


def test_dtype_minus(cases):
    for t1, t2, expected in cases:
        assert dtype_minus(t1, t2) == expected
