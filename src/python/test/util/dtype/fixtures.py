import numpy as np
import pytest
from mvpoly.util.dtype import (
    dtype_add,
    dtype_minus,
    dtype_mul
)


@pytest.fixture
def cases():
    return [
        (int,        int,        int),
        (float,      int,        float),
        (int,        float,      float),
        (float,      float,      float),
        (np.float32, np.float32, np.float32),
        (np.float64, np.float32, np.float64),
        (np.float32, np.float64, np.float64),
        (np.float64, np.float64, np.float64),
    ]


def test_dtype_add(cases):
    for t1, t2, expected in cases:
        assert dtype_add(t1, t2) == expected


def test_dtype_minus(cases):
    for t1, t2, expected in cases:
        assert dtype_minus(t1, t2) == expected


def test_dtype_mul(cases):
    for t1, t2, expected in cases:
        assert dtype_mul(t1, t2) == expected
