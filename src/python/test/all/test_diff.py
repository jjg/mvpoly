from . suite import cls
import pytest
import numpy as np


@pytest.fixture(params=[int, float])
def dtype(request):
    return request.param


def test_0d(cls):
    p = 3 * cls.one(dtype=int)
    assert p.diff(1) == 0


def test_1d(cls):
    x, = cls.variables(1)
    p = x**5 + 2*x**2 + 1
    assert p.diff(1) == 5*x**4 + 4*x


def test_2d(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 + x*y + y**3
    assert p.diff(1, 0) == 2*x + y
    assert p.diff(0, 1) == x + 3*y**2


def test_dtype(cls, dtype):
    p = cls.zero(dtype=dtype)
    p[2, 0] = 4
    p[0, 2] = 3
    assert p.diff(2, 1).dtype == dtype
