from . suite import cls
import pytest
import numpy as np


def relerr(a, b, eps=1e-9):
    return abs(a - b) < abs(b) * eps


def test_zero(cls):
    p = cls.zero(dtype=complex)
    assert p.norm_L(1) == 0
    assert p.norm_L(2) == 0


def test_one(cls):
    p = cls.one(dtype=complex)
    assert relerr(p.norm_L(1), 1)
    assert relerr(p.norm_L(2), 1)


def test_univariate(cls):
    x, = cls.variables(1, dtype=complex)
    p = x + 1
    assert relerr(p.norm_L(1), 4 / np.pi)
    assert relerr(p.norm_L(2), np.sqrt(2))
    assert relerr(p.norm_L(np.inf), 2)


def test_bivariate(cls):
    x, y = cls.variables(2, dtype=complex)
    p = x + y + 1
    # numerical values confirmed Wolfram Alpha
    assert relerr(p.norm_L(2), 1.73205080756)
    assert relerr(p.norm_L(4), 1.96798967126)


def test_trivariate(cls):
    x, y, z = cls.variables(3, dtype=complex)
    p = x + y + z + 1
    # numerical value confirmed by Maple 13
    assert relerr(p.norm_L(2), 2)


def test_bad_n(cls):
    x, y, z = cls.variables(3, dtype=complex)
    p = x + y + z + 1
    with pytest.raises(ValueError):
        p.norm_L(2, 2)
