from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    p = cls.bell_partial(6, 2)
    assert p.degrees == (1, 1, 2, 1, 1)


def test_coefs(cls):
    x = cls.variables(5)
    p = cls.bell_partial(6, 2)
    q = 6*x[4]*x[0] + 15*x[3]*x[1] + 10*x[2]**2
    assert p == q


def test_bad_args(cls):
    with pytest.raises(ValueError):
        cls.bell_partial(2, 6)
    with pytest.raises(ValueError):
        cls.bell_partial(6, -3)
