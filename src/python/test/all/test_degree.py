from . suite import cls
import numpy as np


def test_zero(cls):
    p = cls.zero(dtype=int)
    assert p.degree == -1


def test_0d(cls):
    one = cls.one(dtype=int)
    assert (5*one).degree == 0


def test_1d(cls):
    x, = cls.variables(1, dtype=int)
    p = x**5 + x + 3
    assert p.degree == 5


def test_2d(cls):
    x, y = cls.variables(2, dtype=int)
    p = (x**5 + x + 3) * y**2 + y
    assert p.degree == 7
