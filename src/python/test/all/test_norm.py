from . suite import cls
import pytest
import numpy as np


def test_zero(cls):
    p = cls.zero(dtype=int)
    assert p.height == 0
    assert p.length == 0
    assert p.norm(1) == 0
    assert p.norm(2) == 0
    assert p.norm(np.inf) == 0


def test_simple(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 - y**2
    assert p.norm(1) == 2
    assert abs(p.norm(2) - np.sqrt(2)) < 1e-10
    assert p.norm(np.inf) == 1


def test_height_simple(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 + 5*x*y - 6
    assert p.height == 6


def test_length_simple(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 - 2*x*y**9 + 3
    assert p.length == 6
