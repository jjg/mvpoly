from . suite import cls
import pytest
import numpy as np


# we test a selection of the scipy.special polys, checking
# that they agree with the classical definition

def test_legendre_size(cls):
    for n in range(1, 10):
        p = cls.legendre(n)
        assert p.degrees == (n,)


def test_legendre_explicit(cls):
    x, = cls.variables(1)
    p = cls.legendre(5)
    q = (63*x**5 - 70*x**3 + 15*x)*(1./8)
    assert (p - q).norm(np.inf) < 1e-10


def test_hermitenorm_explicit(cls):
    x, = cls.variables(1)
    p = cls.hermitenorm(5)
    q = x**5 - 10*x**3 + 15*x
    assert (p - q).norm(np.inf) < 1e-10


def test_laguerre_explicit(cls):
    x, = cls.variables(1)
    p = cls.laguerre(3)
    q = (1./6)*(-x**3 + 9*x**2 - 18*x + 6)
    assert (p - q).norm(np.inf) < 1e-10
