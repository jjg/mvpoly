from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    p = cls.chsym(4, 2)
    assert p.degrees == (2, 2, 2, 2)


def test_explicit_1(cls):
    p = cls.chsym(1, 4)
    x, = cls.variables(1)
    q = x**4
    assert p == q


def test_explicit_2(cls):
    p = cls.chsym(2, 2)
    x, y = cls.variables(2)
    q = x*x + x*y + y*y
    assert p == q


def test_n_zero(cls):
    for k in (1, 2, 10):
        p = cls.chsym(0, k)
        assert p == cls.zero()


def test_n_negative(cls):
    with pytest.raises(ValueError):
        cls.chsym(-3, 0)


def test_k_zero(cls):
    for n in (0, 1, 3, 10):
        p = cls.chsym(n, 0)
        assert p == cls.one()


def test_k_negative(cls):
    with pytest.raises(ValueError):
        cls.chsym(3, -1)


# the order k complete homogenwous polynomial in n variables
# evaluated at 1, .., n is the Stirling number { (n + k) / n }
# so for n = 3, k = 2 this is { 5 / 3 } = 25

def test_stirling(cls):
    p = cls.chsym(3, 2)
    assert p(1, 2, 3) == 25
