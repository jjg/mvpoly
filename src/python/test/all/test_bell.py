from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    p = cls.bell(4)
    assert p.degrees == (4, 2, 1, 1)


def test_coefs(cls):
    x1, x2, x3, x4 = cls.variables(4)
    p = cls.bell(4)
    q = x1**4 + 6*x2*x1**2 + 4*x1*x3 + 3*x2**2 + x4
    assert p == q


def test_args(cls):
    with pytest.raises(ValueError):
        cls.bell(-1)
