from . suite import cls


def test_repr_example(cls):
    x, y = cls.variables(2)
    p = (x + 2*y - 4)**3
    assert repr(p)
