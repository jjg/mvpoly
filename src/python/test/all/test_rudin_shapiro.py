from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    for n in range(3, 6):
        p = cls.rudin_shapiro(n)
        assert p.degrees == (2**n-1,)


def test_dtype_default(cls):
    p = cls.rudin_shapiro(3)
    assert p.dtype == int


def test_dtype_user(cls):
    p = cls.rudin_shapiro(3, dtype=complex)
    assert p.dtype == complex


def test_2(cls):
    x, = cls.variables(1, dtype=int)
    p = cls.rudin_shapiro(2)
    q = 1 + x + x**2 - x**3
    assert p == q


# This inequality is what makes these polynomials interesting,
# they have rather small maximum modulus

def test_maxmod(cls):
    eps = 1e-10
    for m in range(2, 7):
        p = cls.rudin_shapiro(m).astype(complex)
        M, _, _, _ = p.maxmodnd(epsilon=eps)
        assert M * (1 - eps) < np.sqrt(2**(m + 1))


# Littlewood, if n = 2^k - 1, P the Rudin-Shapiro polynomial
# order n, then
#
#       ||P||_L4^4  =  (1/3)((4n+1)^2 - (-1)^k (n+1))
#
# Computational Excursions in Analysis and Number Theory
# Peter Borwein

def test_L4_norm(cls):
    for k in [2, 3, 4]:
        expected = (4.0**k) * (4.0 - (-0.5)**k)/3.0
        p = cls.rudin_shapiro(k).astype(complex)
        assert abs(p.norm_L(4)**4 - expected) < 1e-9
