from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    p = cls.bernstein((3, 2), (5, 7), dtype=int)
    assert p.degrees == (5, 7)


def test_badargs(cls):
    with pytest.raises(ValueError):
        cls.bernstein((3, 8), (5, 7))
    with pytest.raises(ValueError):
        cls.bernstein((1, 2, 3), (5, 7))


def test_explicit_1d(cls):
    p = cls.bernstein(3, 4, dtype=int)
    x = cls.variable(0, 1, dtype=int)
    q = 4 * x**3 * (1 - x)
    assert p == q


def test_dtype(cls):
    for dt in [int, float, np.double]:
        p = cls.bernstein((3, 2), (5, 7), dtype=dt)
        assert p.dtype == dt
