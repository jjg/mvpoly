from . suite import cls
import pytest
import numpy as np


def test_bad_args(cls):
    x, y = cls.variables(2)
    p = x**2 + y + 1
    for iv in [[1], [1, 2, 3]]:
        with pytest.raises(RuntimeError):
            p.intd(*iv)


def test_1d(cls):
    x, = cls.variables(1)
    p = x
    for interval in [
            [0, 1],
            (0, 1),
            np.array([0, 1]),
            range(2)
    ]:
        assert p.intd(interval) == 0.5


def test_2d(cls):
    x, y = cls.variables(2)
    p = x**2 + 4*y
    assert p.intd([11, 14], [7, 10]) == 1719


def test_3d_a(cls):
    x, y, z = cls.variables(3)
    p = x + y + z
    assert p.intd([0, 1], [0, 1], [0, 1]) == 1.5


def test_3d_b(cls):
    x, y, z = cls.variables(3)
    p = 3*x**2 + y + z
    assert p.intd([0, 1], [2, 4], [-2, 0]) == 12
