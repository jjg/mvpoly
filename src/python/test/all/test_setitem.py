from . suite import cls
import pytest
import numpy as np


@pytest.fixture(params=[np.int64, np.int32, float])
def dtype(request):
    return request.param


def test_univariate_zero(cls):
    p = cls.zero(dtype=int)
    q = cls.one(dtype=int)
    q[0] = 0
    assert p == q


def test_univariate_one(cls):
    p = cls.zero(dtype=int)
    q = cls.one(dtype=int)
    p[0] = 1
    assert p == q


def test_constant(cls):
    p = cls.zero(dtype=int)
    p[0, 0, 0] = 3
    q = 3 * cls.one(dtype=int)
    assert p == q


def test_variable(cls, dtype):
    x, y, z = cls.variables(3, dtype=dtype)
    q = 2 * x**3 * y**2 * z**4
    p = cls.zero(dtype=dtype)
    p[3, 2, 4] = 2
    assert p == q
    assert p.dtype == dtype
