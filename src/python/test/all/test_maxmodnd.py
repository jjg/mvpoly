from . suite import cls
import pytest
import numpy as np
import itertools
from mvpoly.cube import MVPolyCube


def releps(p, b, eps):
    return np.abs(p.maxmodnd(epsilon=eps)[0] - b) < eps * b


def test_univariate(cls):
    x, = cls.variables(1, dtype=complex)
    p = x**10 + 1
    assert releps(p, 2, 1e-9)


def test_bivariate(cls):
    x, y = cls.variables(2, dtype=complex)
    p = (1 + x**9)*y**9 + 8
    assert releps(p, 10, 1e-9)


# N. Varopoulos, On an inequality of von Neumann and an
# application of themetric theory of tensor products to
# operator theory, J. Funct. Anal., 16 (1974), 83--100.
#
# Note that the maximum modulus here is attained on a
# continuum on the ball: a large eps is needed to halt
# the algorithm before exponential blowup

def test_kaijser_varopoulos(cls):
    x, y, z = cls.variables(3, dtype=complex)
    p = x**2 + y**2 + z**2 - 2*x*y - 2*x*z - 2*y*z
    assert releps(p, 5, 1e-5)


# Rajeev Gupta, Samya Kumar Ray, "On a question of
# N. Th. Varopoulos" Lemma 2.3 states that if a non-negative
# definite matrix A defines a polynomial p = (z, Az), then
# the maximum modulus of the polynomial on the polydisk is
# attained on a corner of the l-infinty unit ball, i.e., for
# z = (z1, z2, ...) and zi = +/-1.
#
# We generate a random 5x5 matrix A, form A^T A (which is
# positive semi-definite) and check that this Lemmas is
# honoured

def test_gupta_ray(cls):
    n = 5
    A = np.random.rand(n, n)
    S = A.T * A
    p = MVPolyCube(S, dtype=complex).asclass(cls)
    expected = 0
    for ztup in itertools.product(*((-1, 1) for _ in range(n))):
        z = np.array(ztup)
        zSz = np.dot(z, np.dot(S, z))
        if zSz > expected:
            expected = zSz
    assert releps(p, expected, 1e-9)
