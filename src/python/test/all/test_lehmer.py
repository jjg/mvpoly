from . suite import cls
import pytest
import numpy as np


def test_size(cls):
    p = cls.lehmer(dtype=int)
    assert p.degrees == (10,)


def test_dtype(cls):
    for dt in [int, float, np.double]:
        p = cls.lehmer(dtype=dt)
        assert p.dtype == dt


def test_explicit(cls):
    p = cls.lehmer(dtype=int)
    x, = cls.variables(1, dtype=int)
    q = x**10 + x**9 - x**7 - x**6 - x**5 - x**4 - x**3 + x + 1
    assert p == q
