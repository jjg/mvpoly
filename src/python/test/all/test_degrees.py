from . suite import cls
import numpy as np


def test_zero(cls):
    p = cls.zero(dtype=np.int32)
    assert p.degrees == ()


def test_constant(cls):
    p = 3 * cls.one(dtype=np.int32)
    assert p.degrees == ()


def test_1d(cls):
    x, = cls.variables(1)
    p = (x+1)**2
    assert p.degrees == (2,)


def test_2d(cls):
    x, _, z = cls.variables(3)
    p = x + z**2 + 7
    assert p.degrees == (1, 0, 2)
