from . suite import cls
import pytest
import numpy as np


def test_variable_1(cls):
    one = cls.one()
    x, = cls.variables(1)
    assert cls.monomials(1, 0) == [one]
    assert cls.monomials(1, 1) == [one, x]
    assert cls.monomials(1, 2) == [one, x, x*x]


def test_variable_2(cls):
    one = cls.one()
    x, y = cls.variables(2)
    assert cls.monomials(2, 0) == [one]
    assert cls.monomials(2, 1) == [one, x, y]
    assert cls.monomials(2, 2) == [one, x, y, x*x, x*y, y*y]


def test_dtype_default(cls):
    for m in cls.monomials(3, 2):
        assert m.dtype == float


def test_dtype_user(cls):
    for m in cls.monomials(3, 2, dtype=complex):
        assert m.dtype == complex


def test_bad_arg_order(cls):
    with pytest.raises(ValueError):
        cls.monomials(0, 3)


def test_bad_arg_degree(cls):
    with pytest.raises(ValueError):
        cls.monomials(2, -1)
