from . suite import cls
import pytest
import numpy as np


@pytest.fixture
def pi(cls):
    x, y = cls.variables(2, dtype=int)
    return (x + 2*y - 4)**3


@pytest.fixture
def pf(cls):
    x, y = cls.variables(2, dtype=float)
    return (x + 2*y - 4)**3


def test_add(pi, pf):
    for r in [
            pi + pf,
            pf + pi,
            pf + pf,
            1 + pf,
            pf + 1,
            1.0 + pf,
            pf + 0.1,
            1.0 + pi,
            pi + 1.0
    ]:
        assert r.dtype == float
    for r in [pi + pi, pi + 1, 1 + pi]:
        assert r.dtype == int


def test_minus(pi, pf):
    for r in [
            pi - pf,
            pf - pi,
            pf - pf,
            1 - pf,
            pf - 1,
            1.0 - pf,
            pf - 0.1,
            1.0 - pi,
            pi - 1.0
    ]:
        assert r.dtype == float
    for r in [pi - pi, pi - 1, 1 - pi]:
        assert r.dtype == int


def test_mul(pi, pf):
    for r in [
            pi * pf,
            pf * pi,
            pf * pf,
            1 * pf,
            pf * 1,
            1.0 * pf,
            pf * 0.1,
            1.0 * pi,
            pi * 1.0
    ]:
        assert r.dtype == float
    for r in [pi * pi, pi * 1, 1 * pi]:
        assert r.dtype == int


def test_scalar(cls):
    x, = cls.variables(1, dtype=np.int32)
    v = np.int16(99)
    froo = x.__rmul__(v)
    assert froo.dtype == np.int32
