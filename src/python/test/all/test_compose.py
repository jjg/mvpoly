from . suite import cls
import numpy as np


def test_1d(cls):
    x, = cls.variables(1, dtype=int)
    p = x**2 + 1
    q = p.compose(x-1)
    exp = (x-1)**2 + 1
    obt = q
    assert exp == obt


def test_2d(cls):
    x, y = cls.variables(2, dtype=int)
    p = x**2 + y + 1
    q = p.compose(2*y, x)
    exp = (2*y)**2 + x + 1
    obt = q
    assert exp == obt


def test_distribute_over_eval(cls):
    x, y = cls.variables(2, dtype=int)
    p = (x + 3*y - 1)**2
    u = x - y
    v = x + y
    for i in range(5):
        for j in range(5):
            exp = p.compose(u, v).eval(i, j)
            obt = p.eval(u.eval(i, j), v.eval(i, j))
            assert exp == obt


def test_call(cls):
    x, y = cls.variables(2, dtype=int)
    p = x + y
    assert isinstance(p(1, y), cls)
    assert isinstance(p(1, 2), np.int64)


def test_1of2(cls):
    x, y = cls.variables(2, dtype=int)
    p = x + 1
    q = p.compose(y, x)
    assert q(1, 0) == 1


def test_diff_nvar(cls):
    # one should be able to convert a 1-d polynomial in one
    # variable to a 1-d polynomial in another; compose() should
    # do this and we test that this is case ...
    _, y = cls.variables(2, dtype=int)
    px = cls.lehmer()
    py = px(y)
    assert px.degrees == (10,)
    assert py.degrees == (0, 10)
