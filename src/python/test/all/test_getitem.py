from . suite import cls
import numpy as np


def test_numargs_zero(cls):
    p = cls.zero(dtype=np.int32)
    assert p[()] == 0
    assert p[0] == 0
    assert p[0, 0] == 0


def test_numargs_const(cls):
    p = cls.one(dtype=np.int32) * 7
    assert p[()] == 7
    assert p[0] == 7
    assert p[3] == 0
    assert p[0, 0] == 7
    assert p[7, 9] == 0


def test_numargs_univar(cls):
    x, = cls.variables(1, dtype=np.int32)
    p = 4 + 3*x + 2*x**2
    assert p[()] == 4
    assert p[0] == 4
    assert p[1] == 3
    assert p[2] == 2
    assert p[3] == 0
    assert p[0, 0] == 4
    assert p[7, 9] == 0


def test_numargs_bivar(cls):
    x, y = cls.variables(2, dtype=np.int32)
    p = 4 + 3*y + 2*x**2
    assert p[()] == 4
    assert p[0] == 4
    assert p[0, 1] == 3
    assert p[2, 0] == 2
    assert p[2] == 2
