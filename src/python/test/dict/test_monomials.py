from mvpoly.dict import MVPolyDict
import pytest


def test_monomials_constant():
    ps = MVPolyDict.monomials(2, 0)
    assert ps[0].coef == {(): 1}


def test_monomials_linear():
    ps = MVPolyDict.monomials(2, 1)
    assert len(ps) == 3
    assert ps[0].coef == {(): 1}
    assert ps[1].coef == {((0, 1),): 1}
    assert ps[2].coef == {((1, 1),): 1}


def test_monomials_quadratic():
    ps = MVPolyDict.monomials(2, 2)
    assert len(ps) == 6
    assert ps[0].coef == {(): 1}
    assert ps[1].coef == {((0, 1),): 1}
    assert ps[2].coef == {((1, 1),): 1}
    assert ps[3].coef == {((0, 2),): 1}
    assert ps[4].coef == {((0, 1), (1, 1)): 1}
    assert ps[5].coef == {((1, 2),): 1}
