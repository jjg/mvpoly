from mvpoly.dict import MVPolyDict
import pytest


@pytest.fixture
def A():
    p = MVPolyDict(dtype=int)
    p[0, 0] = 1
    p[1, 0] = 2
    p[2, 0] = 3
    return p


@pytest.fixture
def B():
    p = MVPolyDict(dtype=int)
    p[0, 0] = 1
    p[0, 1] = 1
    return p


def test_add1(A, B):
    C = A + B
    obt = [[C[i, j] for i in range(3)] for j in range(2)]
    exp = [[2, 2, 3], [1, 0, 0]]
    assert exp == obt


def test_add2(A):
    C = A + 1
    obt = [C[i] for i in range(3)]
    exp = [2, 2, 3]
    assert exp == obt


def test_add3(A):
    C = 1 + A
    obt = [C[i] for i in range(3)]
    exp = [2, 2, 3]
    assert exp == obt


def test_add4(A):
    C = MVPolyDict()
    for i, Ci in enumerate([2, 1, 0, 3]):
        C[i] = Ci
    D = C + A
    obt = [D[i] for i in range(4)]
    exp = [3, 3, 3, 3]
    assert exp == obt


def test_sub(A, B):
    C = A - B
    obt = [[C[i, j] for i in range(3)] for j in range(2)]
    exp = [[0, 2, 3], [-1, 0, 0]]
    assert exp == obt
