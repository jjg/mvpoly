from mvpoly.dict import MVPolyDict
import pytest


@pytest.fixture
def A():
    p = MVPolyDict(dtype=int)
    p[0] = p[1] = 1
    return p


@pytest.fixture
def B():
    p = MVPolyDict(dtype=int)
    for i in range(2):
        for j in range(3):
            p[i, j] = 1
    return p


def test_multiply_scalar(A, B):
    exp = [2, 2]
    for C in [2 * A, A * 2]:
        obt = [C[n] for n in range(2)]
        assert exp == obt


def test_multiply_1d(A):
    C = A * A
    obt = [C[n] for n in range(3)]
    exp = [1, 2, 1]
    assert exp == obt


def test_multiply_dimension(A, B):
    exp = [[1, 1], [2, 2], [1, 1]]
    for C in [A * B, B * A]:
        obt = [[C[i, j] for j in range(2)] for i in range(3)]
        assert exp == obt


def test_multiply_arithmetic():
    x, y = MVPolyDict.variables(2, dtype=int)
    p = (x + y)*(2*x - y)
    q = 2*x**2 + x*y - y**2
    assert p == q


def test_multiply_complex():
    x, y = MVPolyDict.variables(2, dtype=complex)
    p = (x + y)*(x + 1j*y)
    q = x**2 + (1 + 1j)*x*y + 1j*y**2
    assert p == q
