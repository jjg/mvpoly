from mvpoly.dict import MVPolyDict
import pytest


def test_euler_goldbach():
    """
    Euler's four-square identity, discussed in 1749 in a letter
    from Euler to Goldbach.
    """
    a1, a2, a3, a4, b1, b2, b3, b4 = MVPolyDict.variables(8, dtype=int)
    p = (a1**2 + a2**2 + a3**2 + a4**2) * (b1**2 + b2**2 + b3**2 + b4**2)
    q = (a1*b1 - a2*b2 - a3*b3 - a4*b4)**2 + \
        (a1*b2 + a2*b1 + a3*b4 - a4*b3)**2 + \
        (a1*b3 - a2*b4 + a3*b1 + a4*b2)**2 + \
        (a1*b4 + a2*b3 - a3*b2 + a4*b1)**2
    assert p == q
