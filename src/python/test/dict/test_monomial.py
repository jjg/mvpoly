from mvpoly.dict import MVPolyDictMonomial
import pytest


def test_construct_from_empty():
    p = MVPolyDictMonomial()
    assert p.dict == {}


def test_construct_from_dict():
    d = {0: 3, 5: 2}
    p = MVPolyDictMonomial(d)
    assert p.dict == d


def test_construct_from_kw_dict():
    d = {0: 3, 5: 2}
    p = MVPolyDictMonomial(dict=d)
    assert p.dict == d


def test_set_dict():
    d = {0: 3, 5: 2}
    p = MVPolyDictMonomial()
    p.dict = d
    assert p.dict == d


def test_construct_from_key():
    key = ((0, 3), (5, 2))
    p = MVPolyDictMonomial(key=key)
    assert p.key == key


def test_set_key():
    key = ((0, 3), (5, 2))
    p = MVPolyDictMonomial()
    p.key = key
    assert p.key == key


def test_construct_from_kw_index():
    index = (3, 0, 1, 0)
    p = MVPolyDictMonomial(index=index)
    assert p.index_of_length(4) == index
    assert p.index == (3, 0, 1)


def test_set_index():
    index = (3, 0, 1, 0)
    p = MVPolyDictMonomial()
    p.index = index
    assert p.index_of_length(4) == index
    assert p.index == (3, 0, 1)


def test_index_of_zeros():
    for index in [(0), (0, 0), (0, 0, 0)]:
        p = MVPolyDictMonomial(index=index)
        assert p.key == ()


def test_zero_mononomial():
    p = MVPolyDictMonomial(dict={})
    assert p.dict == {}
    assert p.key == ()
    assert p.index_of_length(4) == (0, 0, 0, 0)


def test_nonzero_mononomial():
    d = {0: 3, 3: 1}
    p = MVPolyDictMonomial(dict=d)
    assert p.dict == d
    assert p.key == ((0, 3), (3, 1))
    assert p.index_of_length(4) == (3, 0, 0, 1)


def test_multiply_mononomial():
    p1 = MVPolyDictMonomial(index=(2, 0, 0, 1))
    p2 = MVPolyDictMonomial(index=(0, 2, 0, 1))
    assert (p1 * p2).index_of_length(4) == (2, 2, 0, 2)


def test_occurences():
    p = MVPolyDictMonomial(index=(3, 2))
    assert p.occurences == (0, 0, 0, 1, 1)


def test_degree():
    for pair in [(0, ()), (3, (3,)), (8, (7, 1, 0))]:
        degree, index = pair
        p = MVPolyDictMonomial(index=index)
        assert p.degree == degree
