from mvpoly.dict import MVPolyDict
import pytest


def test_nonzero_enumerate():
    x, y = MVPolyDict.variables(2)
    p = 3*x + y**2 + 7
    expected = [((), 7.0), ((1,), 3.0), ((0, 2), 1.0)]
    assert sorted(p.nonzero) == sorted(expected)
