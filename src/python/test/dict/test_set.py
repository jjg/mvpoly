from mvpoly.dict import MVPolyDict
import pytest


def test_set_constant():
    p = MVPolyDict()
    p[0] = 3
    assert p[0] == 3
    assert p[0, 0] == 3
    assert p.degrees == ()


def test_unset_get():
    p = MVPolyDict()
    assert p[1, 2, 3] == 0


def test_set_get():
    p = MVPolyDict()
    p[3, 2, 1] = 3
    assert p[3, 2, 1] == 3
