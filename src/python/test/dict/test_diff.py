from mvpoly.dict import MVPolyDict
import pytest


def test_diff_invariant_regression():
    x, y = MVPolyDict.variables(2, dtype=int)
    p = x + 2*y
    exp = p.coef.copy()
    _ = p.diff(1, 0)
    obt = p.coef
    assert exp == obt
