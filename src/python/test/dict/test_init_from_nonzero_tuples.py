from mvpoly.dict import MVPolyDict
import pytest


def test_init_from_nonzero_tuples():
    x, y = MVPolyDict.variables(2)
    p = 3*x + y**2 + 7
    q = MVPolyDict.init_from_nonzero_tuples(p.nonzero, p.dtype)
    assert p == q
