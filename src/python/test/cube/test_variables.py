from mvpoly.cube import MVPolyCube
import pytest


def test_count():
    for n in [2, 3, 4]:
        M = MVPolyCube.variables(n)
        assert len(M) == n


def test_create():
    x, y, z = MVPolyCube.variables(3)
    assert (x.coef == [[[0]], [[1]]]).all()
    assert (y.coef == [[[0], [1]]]).all()
    assert (z.coef == [[[0, 1]]]).all()


def test_build():
    x, y = MVPolyCube.variables(2)
    p = 2*x**2 + 3*x*y + 1
    assert (p.coef == [[1, 0], [0, 3], [2, 0]]).all()


def test_dtype():
    x, y = MVPolyCube.variables(2, dtype=int)
    p = 2*x**2 + 3*x*y + 1
    for m in (x, y, p):
        assert m.dtype == int
        assert m.coef.dtype == int
