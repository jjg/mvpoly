from mvpoly.cube import MVPolyCube
import pytest


# diff should return the derivative polynomial and not
# modify the coefficient array,

def test_not_mutated():
    x, y = MVPolyCube.variables(2, dtype=int)
    p = x + 2*y
    coef_before = p.coef.copy()
    p.diff(1, 0)
    coef_after = p.coef
    assert (coef_after == coef_before).all()
