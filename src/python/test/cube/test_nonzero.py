from mvpoly.cube import MVPolyCube
import pytest


def test_zero():
    p = MVPolyCube.zero()
    assert p.nonzero == []


def test_one():
    p = MVPolyCube.one()
    assert p.nonzero == [((0,), 1)]


def test_enumerate():
    x, y = MVPolyCube.variables(2)
    p = 3*x + y**2 + 7
    obt = p.nonzero
    exp = [((0, 0), 7.0), ((1, 0), 3.0), ((0, 2), 1.0)]
    assert sorted(obt) == sorted(exp)
