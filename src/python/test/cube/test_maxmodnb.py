from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


# this will not be released for a while, it is here just to
# check that mvpoly integration works for the author

def have_maxmodnb():
    try:
        import maxmodnb
    except ImportError:
        return False
    return True


with_maxmodnb = pytest.mark.skipif(
    not have_maxmodnb(),
    reason='maxmodnb module not presnt'
)


@pytest.fixture
def p():
    x, _ = MVPolyCube.variables(2, dtype=complex)
    return x**2 + 1


@with_maxmodnb
def test_simple(p):
    eps = 1e-10
    expected = 2.0
    obtained = p.maxmodnb(eps=eps)[0]
    assert abs(expected - obtained) < eps * expected


@with_maxmodnb
def test_fifomax(p):
    with pytest.raises(RuntimeError):
        p.maxmodnb(fifomax=3)


@with_maxmodnb
def test_unknown_keyword(p):
    with pytest.raises(ValueError):
        p.maxmodnb(nosuchvar=3)


@with_maxmodnb
def test_no_positional_args(p):
    with pytest.raises(TypeError):
        p.maxmodnb(3)
