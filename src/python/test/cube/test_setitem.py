from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


# regression in __setitem__() : when passed an empty
# tuple the value was broadcast to the whole array,
# we don't want that

def test_broadcast_regression():
    p = MVPolyCube(np.zeros((3, 3), dtype=int), dtype=int)
    p[()] = 1
    q = MVPolyCube.one(dtype=int)
    assert p == q
