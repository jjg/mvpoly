from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


@pytest.fixture
def r():
    return MVPolyCube.variables(1, dtype=float)[0]


# these values are given exactly in Table 1 of Wendland, "Error
# estimates for interpolation by compactly supported radial
# basis functions of minimal degree", J. Approx. Theory, 93 (2),
# 1998, 258--272

def test_dim1_order0(r):
    expected = 1 - r
    obtained = MVPolyCube.wendland(1, 0)
    assert expected == obtained


def test_dim3_order0(r):
    expected = (1 - r)**2
    obtained = MVPolyCube.wendland(3, 0)
    assert expected == obtained


def test_dim5_order0(r):
    expected = (1 - r)**3
    obtained = MVPolyCube.wendland(5, 0)
    assert expected == obtained


# these are given only "up to a constant" op. cit., so we use a
# custom assert -- if two polynomials are equal up to a constant
# then the ratios of samples of those polynomials will be that
# constant, we check that rmin/rmax =~ 1.0

def proportionate(p, q, eps=1e-10):
    xs = [0.1, 0.3, 0.5, 0.7, 0.9]
    ratios = [p(x)/q(x) for x in xs]
    rmax = max(ratios)
    rmin = min(ratios)
    return abs((rmax - rmin)/rmax) < eps


def test_dim1_order1(r):
    expected = (3*r + 1) * (1 - r)**3
    obtained = MVPolyCube.wendland(1, 1)
    assert proportionate(expected, obtained)


def test_dim1_order2(r):
    expected = (8*r*r + 5*r + 1) * (1 - r)**5
    obtained = MVPolyCube.wendland(1, 2)
    assert proportionate(expected, obtained)


def test_dim3_order1(r):
    expected = (4*r + 1) * (1 - r)**4
    obtained = MVPolyCube.wendland(3, 1)
    assert proportionate(expected, obtained)


def test_dim3_order2(r):
    expected = (35*r*r + 18*r + 3) * (1 - r)**6
    obtained = MVPolyCube.wendland(3, 2)
    assert proportionate(expected, obtained)


def test_dim3_order3(r):
    expected = (32*r*r*r + 25*r*r + 8*r + 1) * (1 - r)**8
    obtained = MVPolyCube.wendland(3, 3)
    assert proportionate(expected, obtained, eps=1e-7)


def test_wendland_dim5_order1(r):
    expected = (5*r + 1) * (1 - r)**5
    obtained = MVPolyCube.wendland(5, 1)
    assert proportionate(expected, obtained)


def test_wendland_dim5_order2(r):
    expected = (16*r*r + 7*r + 1) * (1 - r)**7
    obtained = MVPolyCube.wendland(5, 2)
    assert proportionate(expected, obtained, eps=1e-8)
