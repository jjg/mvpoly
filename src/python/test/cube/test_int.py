from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


# integration behaves stangely for int, since integer division
# is used (perhaps we should error)

@pytest.fixture(params=[float, complex])
def p(request):
    shp = (6, 7, 8)
    c = np.random.randint(low=-10, high=10, size=shp)
    return MVPolyCube(c, dtype=request.param)


def test_inverse_of_diff(p):
    expected = p.coef
    obtained = p.int(1, 1, 2).diff(1, 1, 2).coef
    assert (np.abs(expected - obtained) < 1e-10).all()
