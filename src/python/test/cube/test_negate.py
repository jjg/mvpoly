from mvpoly.cube import MVPolyCube
import pytest


def test_negation():
    x, y = MVPolyCube.variables(2)
    p = 2*x**2 - 3*x*y + 1
    assert ((-p).coef == [[-1, 0], [0, 3], [-2, 0]]).all()
