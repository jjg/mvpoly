from mvpoly.cube import MVPolyCube
import pytest


@pytest.fixture
def A():
    return MVPolyCube([1, 2, 3])


@pytest.fixture
def B():
    return MVPolyCube([[1], [1]])


def test_poly_poly(A, B):
    obtained = (A + B).coef
    expected = [[2, 2, 3],
                [1, 0, 0]]
    assert (expected == obtained).all()


def test_poly_scalar(A):
    obtained = (A + 1).coef
    expected = [2, 2, 3]
    assert (expected == obtained).all()


def test_scalar_poly(A):
    obtained = (1 + A).coef
    expected = [2, 2, 3]
    assert (expected == obtained).all()
