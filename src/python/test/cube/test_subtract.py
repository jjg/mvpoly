from mvpoly.cube import MVPolyCube
import pytest


def test_subtract():
    x, _ = MVPolyCube.variables(2)
    p = 1 - x
    q = -(x - 1)
    assert (p.coef == q.coef).all()
