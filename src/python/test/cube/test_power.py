from mvpoly.cube import MVPolyCube
import pytest


def test_univariate_square():
    A = MVPolyCube([1, 1])
    obtained = (A**2).coef
    expected = [1, 2, 1]
    assert (expected == obtained).all()


def test_univariate_cube():
    A = MVPolyCube([1, 1])
    obtained = (A**3).coef
    expected = [1, 3, 3, 1]
    assert (expected == obtained).all()


def test_dtype():
    A = MVPolyCube([1, 1], dtype=int)
    obtained = (A**5).coef
    assert obtained.dtype == int


def test_power_non_integer():
    A = MVPolyCube([1, 1])
    with pytest.raises(TypeError):
        A**1.5


def test_power_negative():
    A = MVPolyCube([1, 1])
    with pytest.raises(ArithmeticError):
        A**-2
