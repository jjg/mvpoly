from mvpoly.cube import MVPolyCube
import numpy as np
import pytest


def test_self():
    p = MVPolyCube(
        [[1, 2, 3],
         [4, 5, 6]]
    )
    assert p == p


def test_diffsize():
    p = MVPolyCube(
        [[1, 2, 0],
         [4, 5, 0],
         [0, 0, 0]]
    )
    q = MVPolyCube(
        [[1, 2],
         [4, 5]]
    )
    assert p == q


def test_diffdim():
    p = MVPolyCube(
        [[1, 2, 3],
         [0, 0, 0]]
    )
    q = MVPolyCube([1, 2, 3])
    assert p == q


def test_unequal():
    p = MVPolyCube([1, 2, 3])
    q = MVPolyCube([1, 2, 3, 4])
    assert not p == q
    assert p != q
