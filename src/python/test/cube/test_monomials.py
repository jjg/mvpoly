from mvpoly.cube import MVPolyCube
import pytest


def test_constant():
    L = MVPolyCube.monomials(2, 0)
    assert len(L) == 1
    assert (L[0].coef == [[1]]).all()


def test_linear():
    L = MVPolyCube.monomials(2, 1)
    assert len(L) == 3
    assert (L[0].coef == [[1]]).all()
    assert (L[1].coef == [[0, 0],
                          [1, 0]]).all()
    assert (L[2].coef == [[0, 1],
                          [0, 0]]).all()


def test_quadratic():
    L = MVPolyCube.monomials(2, 2)
    assert len(L) == 6
    assert (L[0].coef == [[1]]).all()
    assert (L[1].coef == [[0, 0],
                          [1, 0]]).all()
    assert (L[2].coef == [[0, 1],
                          [0, 0]]).all()
    assert (L[3].coef == [[0, 0, 0],
                          [0, 0, 0],
                          [1, 0, 0]]).all()
    assert (L[4].coef == [[0, 0, 0],
                          [0, 1, 0],
                          [0, 0, 0]]).all()
    assert (L[5].coef == [[0, 0, 1],
                          [0, 0, 0],
                          [0, 0, 0]]).all()
