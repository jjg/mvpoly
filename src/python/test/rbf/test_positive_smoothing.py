from . suite import rbf_class, poly_class
import pytest
import numpy as np


# When using a small positive smoothing parameter on a coarsely
# discretised step, we expect nodes away from the step to be
# close (but not that close) to the data samples. (This test will
# fail if the the `sign` in the smoothing setup is reversed)


def test_positive_smoothing(rbf_class, poly_class):
    eps = 0.03
    n = 10
    x = np.linspace(0, n-1, n)
    f = np.array([-1 if k < n/2 else 1 for k in x], dtype=float)
    rbf = rbf_class(x, f, smooth=1e-4, poly_class=poly_class)
    offstep = list(range(0, n//2-1)) + list(range(n//2+1, n))
    x_offstep = x[offstep]
    f0 = rbf(x_offstep)
    f1 = f[offstep]
    assert np.allclose(f0, f1, atol=eps)
