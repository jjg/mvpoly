from . suite import rbf_class
import pytest
import numpy as np


def test_poly_dtype_real(rbf_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_class(x, f)
    fi = rbf(5.0)
    assert fi.dtype == np.float64


def check_poly_dtype_complex(rbf_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x) * 1j
    rbf = rbf_class(x, f)
    fi = rbf(5.0)
    assert fi.dtype == np.complex128
