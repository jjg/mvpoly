from . suite import poly_class, rbf_eps_class
import pytest
import numpy as np


# Check that an RBF instances of classes which use the epsilon
# parameter can be constructed using a None value of (and that
# results in epsilon being chosen non-zero)


def test_epsilon_auto(rbf_eps_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_eps_class(x, f, epsilon=None)
    assert rbf.epsilon > 0


# Check that an RBF instance with default epsilon/radius is not
# subject to overshoot


def test_epsilon_stability(rbf_eps_class, poly_class):
    np.random.seed(1234)
    x = np.linspace(0, 10, 50)
    f = x + 4.0 * np.random.randn(len(x))
    rbf = rbf_eps_class(x, f, poly_class=poly_class)
    x0 = np.linspace(0, 10, 1000)
    f0 = rbf(x0)
    assert np.abs(f0-x0).max() / np.abs(f-x).max() < 1.2
