from . suite import poly_class, rbf_radius_class
import pytest
import numpy as np


# Check that an RBF instance can be constructed using
# with radius absent or a None value (and that results
# in radius being chosen non-zero)


def test_radius_none(rbf_radius_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_radius_class(x, f, radius=None)
    assert rbf.radius > 0


def test_radius_absent(rbf_radius_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_radius_class(x, f)
    assert rbf.radius > 0
