from . suite import rbf_class, poly_class
import pytest
import numpy as np
from mvpoly.rbf import RBFWendland


@pytest.fixture(params=[True, False])
def large(request):
    return request.param


# Check that an RBF interpolates the nodes (1D)


def test_on_nodes_1d(rbf_class, poly_class, large):
    eps = 1e-10
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_class(x, f, poly_class=poly_class)
    fi = rbf(x, large=large)
    assert np.allclose(f, fi, atol=eps)


# Check that an RBF interpolates the nodes (2D)


def test_on_nodes_2d(rbf_class, poly_class, large):
    if rbf_class == RBFWendland:
        pytest.skip('No complex for Wndland')
    eps = 1e-10
    x = np.random.rand(50, 1)*4 - 2
    y = np.random.rand(50, 1)*4 - 2
    f = x*np.exp(-x**2 - 1j*y**2)
    rbf = rbf_class(x, y, f, poly_class=poly_class)
    fi = rbf(x, y, large=large)
    fi.shape = x.shape
    assert np.allclose(f, fi, atol=eps)


# Check that an RBF instance approximates a smooth function
# away from the nodes.


def test_off_nodes_1d(rbf_class, poly_class, large):
    if rbf_class == RBFWendland:
        pytest.skip('FIXME')
    atol = 0.15
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_class(x, f, poly_class=poly_class)
    x_fine = np.linspace(0, 10, 100)
    f0 = rbf(x_fine, large=large)
    f1 = np.sin(x_fine)
    assert np.allclose(f0, f1, atol=atol)
