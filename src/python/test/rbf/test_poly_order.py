from . suite import poly_class, rbf_class
import pytest
import numpy as np


# check that an RBF instance with poly_order=None has a poly
# method of None, an that the rbf method is identical to the
# call method


def test_poly_order_none(rbf_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_class(x, f, poly_order=None)
    assert rbf.poly is None
    assert rbf.rbf(np.array(5)) == rbf(5)


# check that an RBF instance with specified poly_order has a
# poly method which is at most of that degree


@pytest.mark.parametrize('n', range(4))
def test_poly_order_natural(rbf_class, n):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    rbf = rbf_class(x, f, poly_order=n)
    assert rbf.poly is not None
    assert rbf.poly.degree <= n


# check that attempting to create an RBF instance with a negative
# specified poly_order raises a ValueError


def test_order_negative_implicit(rbf_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    with pytest.raises(ValueError):
        rbf_class(x, f, poly_order=-1)


def test_order_negative_explicit(rbf_class, poly_class):
    x = np.linspace(0, 10, 9)
    f = np.sin(x)
    with pytest.raises(ValueError):
        rbf_class(x, f, poly_order=-1, poly_class=poly_class)
