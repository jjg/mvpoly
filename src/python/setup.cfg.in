[metadata]
name = @PACKAGE_NAME@
version = @PACKAGE_VERSION@
url = @PACKAGE_URL@
author = @PACKAGE_AUTHOR@
author_email = @PACKAGE_AUTHOR_EMAIL@
description = A library for multivariate polynomials
long_description = file: README.rst
long_description_content_type = text/x-rst
keywords =
    polynomial
    multivariate
    numeric
    RBF
license = LGPLv3
classifiers =
    Development Status :: 4 - Beta
    Environment :: Console
    Intended Audience :: Science/Research
    License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)
    Programming Language :: Python
    Programming Language :: Python :: 3
    Operating System :: OS Independent
    Topic :: Scientific/Engineering :: Mathematics
project_urls =
    Documentation = https://mvpoly.readthedocs.io/
    Source = https://gitlab.com/jjg/mvpoly
python_requires =
    >= 3.2

[options]
package_dir =
    = src
packages =
    mvpoly
    mvpoly.util
install_requires =
    numpy
    scipy
zip_safe = True
include_package_data = True

[options.extras_require]
dev =
    build
    twine
    sphinx
    numpydoc
test =
    pytest
    pytest-cov
    mypy
    hypothesis

[options.package_data]
mvpoly =
    README.rst

[egg_info]
egg_base = /tmp

[tool:pytest]
addopts = --cov=mvpoly --cov-report=html:test/coverage --cov-report=term
